

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Tour Dates - Robert Ross - British comedy Historian</title>

<?php include("includes/meta.php"); ?>

<style type="text/css">
table { width: 100%; }
td { padding: 10px 0 10px 10px; }
th.dates-col-1 { width: 17%; }
th.dates-col-2 { width: 28%; }
th.dates-col-3 { width: 25%; }
th.dates-col-4 { width: 10%; }
th.dates-col-5 { width: 10%; }
th.dates-col-6 { width: 10%; }
th { background-color: #ff9000; color: #fff; font-weight: bold; padding: 10px 0 10px 10px; font-size: 120%; }
tr:nth-child(even) {background: #f1f1f1}
tr:nth-child(odd) {background: #fafafa}
.production-name { width: 15%; }
.production-name img { width: 80%; }
</style>
</head>

<body id="body-tourdates">

<div class="container">

<?php include("includes/header.php"); ?>

<?php include("includes/navigation.php"); ?>

<div class="row">
<div id="inside-page" class="col-md-12">
<h2>Tour Dates</h2>

<div class="row links-row">
<div class="col-md-12">
<table class="tour-dates-table">
<tbody>
<tr>
<th class="dates-col-1">
Show
</th>

<th class="dates-col-2">
Venue
</th>

<th class="dates-col-3">
Date
</th>

<th class="dates-col-4">
Time
</th>

<th class="dates-col-5">
Phone Number
</th>

<th class="dates-col-6">
Website
</th>
</tr>

<tr>
<td class="production-name">
<img src="images/hancock-show.png" alt="Tony Hancock - A Celebration">
</td>

<td>
Bradfield Festival, Bradfield College, Berkshire
</td>

<td class="tour-date">
Mon June 25, 2018
</td>

<td class="time">
1:30pm
</td>

<td>
0118 964 4500
</td>

<td>
<a href="https://www.ticketsource.co.uk/event/FGEEDJ">Booking Link</a>
</td>

</tr>

<tr>
<td class="production-name">
<img src="images/yorkshire-fringe-launch.png" alt="The Great Yorkshire Fringe Launch">
</td>

<td>
Leicester Square Theatre
</td>

<td class="tour-date">
Tues July 3, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
020 7734 2222
</td>

<td>
<a href="https://leicestersquaretheatre.ticketsolve.com/shows/873589051">Booking Link</a>
</td>
</tr>





<tr>
<td class="production-name">
<img src="images/what-carry-show.png" alt="Oh, What a Carry On!">
</td>

<td>
The Quay, Sudbury
</td>

<td class="tour-date">
Fri July 6, 2018
</td>

<td class="time">
7:30pm
</td>

<td>
01787 374745
</td>

<td>
<a href="http://www.quaysudbury.com/event/oh-what-carry-fri-2018-07-06-1930">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/what-carry-show.png" alt="Oh, What a Carry On!">
</td>

<td>
The Astor Community Theatre, Deal
</td>

<td class="tour-date">
Sat July 7, 2018
</td>

<td class="time">
7:30pm
</td>

<td>
01304 370220
</td>

<td>
<a href="https://theastor.org/TheAstor.dll/WhatsOn?Programme=1734588">Booking Link</a>
</td>
</tr>




<tr>
<td class="production-name">
<img src="images/liz-fraser-show.png" alt="Liz Fraser: Confessions of a Carry On Girl">
</td>

<td>
The Museum of Comedy
</td>

<td class="tour-date">
Mon July 16, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://museumofcomedy.ticketsolve.com/shows/873588897">Booking Link</a>
</td>
</tr>




<tr>
<td class="production-name">
<img src="images/tony-slattery-show.png" alt="Tony Slattery: Slattery Will Get You Nowhere">
</td>

<td>
The Museum of Comedy
</td>

<td>
Fri July 20 &amp; Sat July 21, 2018
</td>

<td class="tour-date" style="display: none;">
Sat July 21, 2018
</td>

<td class="time">
8:30pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://museumofcomedy.ticketsolve.com/shows/873586833">Booking Link</a>
</td>
</tr>





<tr>
<td class="production-name">
<img src="images/barry-cryer-show.png" alt="Barry Cryer: Name-drops Keep Falling On My Hand">
</td>

<td>
The Museum of Comedy
</td>

<td class="tour-date">
Sun July 22, 2018
</td>

<td class="time">
4:00pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://museumofcomedy.ticketsolve.com/shows/873588898">Booking Link</a>
</td>
</tr>







<tr>
<td class="production-name">
<img src="images/tony-slattery-show.png" alt="Tony Slattery: Slattery Will Get You Nowhere">
</td>

<td>
The Great Yorkshire Fringe, The Teapot
</td>

<td class="tour-date">
Thurs July 26, 2018
</td>

<td class="time">
8:30pm
</td>

<td>
01904 500600
</td>

<td>
<a href="https://greatyorkshirefringe.ticketsolve.com/shows/873586250">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/tony-slattery-show.png" alt="Tony Slattery: Slattery Will Get You Nowhere">
</td>

<td>
The Great Yorkshire Fringe, The White Rose Rotunda
</td>

<td class="tour-date">
Fri July 27, 2018
</td>

<td class="time">
3:00pm
</td>

<td>
01904 500600
</td>

<td>
<a href="https://greatyorkshirefringe.ticketsolve.com/shows/873586250">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/michael-palin-show.png" alt="Michael Palin In Conversation with Robert Ross">
</td>

<td>
The Great Yorkshire Fringe, The Grand Opera House
</td>

<td class="tour-date">
Sat July 28, 2018
</td>

<td class="time">
2:00pm
</td>

<td>
01904 500600
</td>

<td>
<a href="https://greatyorkshirefringe.ticketsolve.com/shows/873586917">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/michael-palin-podcast.png" alt="The Great Yorkshire Fringe Podcast with Michael Palin">
</td>

<td>
The Great Yorkshire Fringe, The White Rose Rotunda
</td>

<td class="tour-date">
Sat July 28, 2018
</td>

<td class="time">
5:00pm
</td>

<td>
01904 500600
</td>

<td>
<a href="https://greatyorkshirefringe.ticketsolve.com/shows/873586259">Booking Link</a>
</td>
</tr>



<tr>
<td class="production-name">
<img src="images/tony-slattery-show.png" alt="Tony Slattery: Slattery Will Get You Nowhere">
</td>

<td>
The Stand Comedy Club, Newcastle
</td>

<td class="tour-date">
Tue July 31, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
0191 300 9700
</td>

<td>
<a href="http://www.thestand.co.uk/show/30543/tony_slattery_slattery_will_get_you_nowhere">Booking Link</a>
</td>
</tr>



<tr>
<td class="production-name">
<img src="images/tony-slattery-improv.png" alt="Tony Slattery's Crimes Against Improv">
</td>

<td>
The Museum of Comedy
</td>

<td>
Sun August 5 &amp; Sun August 12, 2018
</td>

<td class="tour-date" style="display: none;">
Sun August 12, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://leicestersquaretheatre.ticketsolve.com/shows/873588781">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/tony-slattery-show.png" alt="Tony Slattery: Slattery Will Get You Nowhere">
</td>

<td>
The Stand's New Town Theatre, Lower Hall, Edinburgh
</td>

<td>
Everyday from Wed August 15 - Sun August 26, 2018
</td>

<td class="tour-date" style="display: none;">
Sun August 26, 2018
</td>

<td class="time">
1:45pm
</td>

<td>
0131 226 0000
</td>

<td>
<a href="https://tickets.edfringe.com/whats-on/tony-slattery-slattery-will-get-you-nowhere">Booking Link</a>
</td>
</tr>




<tr>
<td class="production-name">
<img src="images/tony-slattery-improv.png" alt="Tony Slattery's Crimes Against Improv">
</td>

<td>
Stand 1, The Stand Comedy Club, 5 York Place, Edinburgh
</td>

<td>
Mon August 20 - Wed August 22 - Fri August 24 - Sun August 26, 2018
</td>

<td class="tour-date" style="display: none;">
Sun August 26, 2018
</td>

<td class="time">
6:30pm
</td>

<td>
0131 226 0000 
</td>

<td>
<a href="https://tickets.edfringe.com/whats-on/tony-slattery-s-crimes-against-improv">Booking Link</a>
</td>
</tr>


<tr>
<td class="production-name">
<img src="images/vaughan-trunk-show.png" alt="Vaughan in a Trunk: My Life on Stage - In Conversation with Rebecca Vaughan">
</td>

<td>
The Swallow Theatre, Newton Stewart
</td>

<td class="tour-date">
Tue August 31, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
01988 850368
</td>

<td>
<a href="https://www.swallowtheatre.co.uk/performances/vaughan-in-a-trunk-my-life-on-stage/">Booking Link</a>
</td>
</tr>



<tr>
<tr>
<td class="production-name">
<img src="images/what-carry-show.png" alt="Oh, What a Carry On!">
</td>

<td>
The Leatherhead Theatre
</td>

<td class="tour-date">
Thurs September 6, 2018
</td>

<td class="time">
7:30pm
</td>

<td>
01372 365141
</td>

<td>
<a href="https://www.theleatherheadtheatre.com/ohwhatacarryon">Booking Link</a>
</td>
</tr>




<tr>
<td class="production-name">
<img src="images/forgotten-heros.png" alt="Forgotten Heroes of Comedy">
</td>

<td>
The Quay, Sudbury
</td>

<td class="tour-date">
Fri September 7, 2018
</td>

<td class="time">
7:30pm
</td>

<td>
01787 374745
</td>

<td>
<a href="https://quaytheatre.ticketsolve.com/shows/873592263">Booking Link</a>
</td>
</tr>




<tr>
<td class="production-name">
<img src="images/tony-slattery-improv.png" alt="Tony Slattery's Crimes Against Improv">
</td>

<td>
The Museum of Comedy
</td>

<td class="tour-date">
Sun September 9, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://leicestersquaretheatre.ticketsolve.com/shows/873588781">Booking Link</a>
</td>
</tr>


<tr>
<td class="production-name">
<img src="images/what-carry-show.png" alt="Oh, What a Carry On!">
</td>
<td>
The Old Laundry Theatre, Bowness-on-Windermere
</td>
<td class="tour-date">
Thurs September 20, 2018
</td>
<td class="time">
8:00pm
</td>
<td>
015394 40872 
</td>
<td>
<a href="https://www.oldlaundrytheatre.co.uk/event/oh-what-a-carry-on/">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/carry-on-jubilee-cruise.png" alt="Carry On! The Diamond Jubilee Celebration Cruise">
</td>

<td>
The Magellan: Cruise &amp; Maritime Voyages Mediterranean Odyssey
</td>

<td>
Sat September 22 - Sun October 7, 2018
</td>

<td class="tour-date" style="display: none;">
Sun October 7, 2018
</td>

<td class="time">
</td>

<td>
0844 998 3788
</td>

<td>
<a href="https://www.cruiseandmaritime.com/cruise/g8c26/mediterranean-odyssey">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/forgotten-heros.png" alt="Forgotten Heroes of Comedy">
</td>

<td>
Ropery Hall, The Ropewalk, Barton upon Humber
</td>

<td class="tour-date">
Thurs October 11, 2018
</td>

<td class="time">
7:30pm
</td>

<td>
01652 660380
</td>

<td>
<a href="http://www.roperyhall.co.uk/2018/04/27/robert-ross-forgotten-heroes-of-comedy/">Booking Link</a>
</td>
</tr>


<tr>
<td class="production-name">
<img src="images/tv-comedy-legends-cruise.png" alt="TV Comedy Legends Cruise">
</td>

<td>
The Columbus: Cruise &amp; Maritime Voyages Canary Islands &amp; Madeira 
</td>

<td>
Fri October 12 - Sat October 27, 2018
</td>

<td class="tour-date" style="display: none;">
Sat October 27, 2018
</td>

<td class="time">
</td>

<td>
0844 998 3788
</td>

<td>
<a href="https://www.cruiseandmaritime.com/cruise/c819/canary-islands-madeira">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/tony-slattery-show.png" alt="Tony Slattery: Slattery Will Get You Nowhere">
</td>

<td>
The Quarry Theatre, Bedford
</td>

<td class="tour-date">
Sat October 27, 2018
</td>

<td class="time">
7:30pm
</td>

<td>
01234 362337
</td>

<td>
<a href="http://www.quarrytheatre.org.uk/shows/slattery/">Booking Link</a>
</td>
</tr>



<tr>
<td class="production-name">
<img src="images/tony-slattery-improv.png" alt="Tony Slattery's Crimes Against Improv">
</td>

<td>
The Museum of Comedy
</td>

<td class="tour-date">
Sun November 4, 2018
</td>

<td class="time">
7:00pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://leicestersquaretheatre.ticketsolve.com/shows/873588781">Booking Link</a>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/remembering-dick-emery.png" alt="Remembering Dick Emery">
</td>
<td>
The Club for Acts and Actors
</td>
<td class="tour-date">
Fri November 16, 2018
</td>
<td class="time">
3:00pm
</td>

<td>
020 7836 3172
</td>
<td>
<a href="http://www.thecaa.org/">Booking Link</a>
</td>
</tr>



<tr>
<td class="production-name">
<img src="images/what-carry-show.png" alt="Oh, What a Carry On!">
</td>

<td>
Stratford Play House,  Stratford-upon-Avon
</td>

<td class="tour-date">
Sat November 17, 2018
</td>

<td class="time">
7:30pm
</td>

<td>

</td>

<td>

</td>
</tr>



<tr>
<td class="production-name">
<img src="images/what-carry-show.png" alt="Oh, What a Carry On!">
</td>

<td>
New Wimbledon Theatre, Wimbledon
</td>

<td class="tour-date">
Sun November 18, 2018
</td>

<td class="time">
3:00pm
</td>

<td>
0844 871 7615
</td>

<td>
</td>
</tr>

<tr>
<td class="production-name">
<img src="images/still-carry-on-jp.png" alt="Jacki Piper: Still Carrying On">
</td>

<td>
The Museum of Comedy
</td>

<td class="tour-date">
Sun November 25, 2018
</td>

<td class="time">
4:00pm
</td>

<td>
020 7534 1744
</td>

<td>
<a href="https://museumofcomedy.ticketsolve.com/shows/873590736">Booking Link</a>
</td>
</tr>
</tbody></table>
</div>
</div> <!-- /.Row -->

</div> <!-- /.Inside-Page -->
</div> <!-- /.Row -->

<?php include("includes/footer.php"); ?>

</div><!-- /.container -->

<?php include("includes/scripts.php"); ?>

<script type="text/javascript">
/* REMOVE DATE IF DATE HAS PASSED */
$(function(){
    $('.tour-date').each(function(key,value){
        var currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        var date = new Date($(value).text());
        if(date < currentDate){
            var currenttable = $(value).parent().closest('table');
            $(value).parent().remove();
            var count = $(currenttable).find('tr').length;
            if(count<=2) { $(currenttable).remove(); }
        }
    });
});
</script>
</body>
</html>
